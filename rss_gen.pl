#!/usr/bin/perl

=head1 SYNOPSIS

    rss_gen.pl [OPTION]... 

    --build
    --encode=FILE
    --debug       
    --help         print this help message

Examples:

    

=head1 DESCRIPTION

Converts an Audio CD into a sinlge MP3 and adds it to an RSS feed.

=head1 AUTHOR

Caleb Vincent

=cut

use strict;
use warnings;
use XML::RSS::SimpleGen;
use Net::Address::IP::Local;
use Getopt::Long;
use Data::Dumper;

my $outFile = "encode";
my $tempDir = "tmp";
my $debug = "debug";
my $build ="build";

my %optionsData = (
    "$outFile"  => '',
    "$tempDir" => "/tmp",
    "$debug" => "",
    "$build" => ""
);

Getopt::Long::Configure(qw(bundling no_getopt_compat auto_help ignore_case));

my $verbose = '';   # option variable with default value (false)
GetOptions (
    "$outFile=s" => \$optionsData{$outFile},
    "$tempDir=s" => \$optionsData{$tempDir},
    "$debug" => \$optionsData{$debug},
    "$build" => \$optionsData{$build},
);

if($optionsData{$debug} ne '')
{ 
    print Dumper(\%optionsData);
}


if ($optionsData{$outFile} ne '')
{
   my $tempWavFile = "$optionsData{$tempDir}/$optionsData{$outFile}.wav"; 
   if(-e $optionsData{$outFile})
   {
      die "File '$optionsData{$outFile}' already exists";
   }
   system("cdparanoia", "1-", $tempWavFile) == 0 or die "Could not rip CD";
   system("lame",  $tempWavFile, $optionsData{$outFile});
   unlink $tempWavFile or warn "Cannot remove temp file $tempWavFile";
}

if($optionsData{$build} ne '')
{
   my $ip = Net::Address::IP::Local->public;
   my $url = "http://$ip:8080/";
   my $feedUrl = $url . 'audiobooks.rss';
   rss_new( "$feedUrl", "Audio Books", "" );

   get_url( $feedUrl );

   for my $file (glob( '*.mp3' ))
   {
      print "Adding: $url\n";
      rss_item($url . $file, $file);
   }

   rss_save( 'audiobooks.rss');
}

exit;
